package com.example.trainbookingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainbookingappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainbookingappApplication.class, args);
	}

}
