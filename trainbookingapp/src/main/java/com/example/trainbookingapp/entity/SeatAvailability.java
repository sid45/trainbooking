package com.example.trainbookingapp.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "seatavailability")
public class SeatAvailability {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long trainDetailsId;

	@ManyToOne
	@JoinColumn(name = "train_id")
	private Train train;

	private LocalDate trainDate;

	private long availableSeats;

}
