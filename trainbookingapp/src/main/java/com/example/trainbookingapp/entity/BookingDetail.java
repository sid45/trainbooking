package com.example.trainbookingapp.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="bookingdetail")
public class BookingDetail {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long bookingId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="train_id")
	private Train train;
	
	private int noOfPassengers;
	
	private LocalDate bookingDate;
	
	private double totalPrice;
	
	private String paymentMode;
	
	
}
