package com.example.trainbookingapp.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="user")
public class User {
	
	@Id
	private long userId;
	
	private String userName;
	
	private String password;
	
	private String email;
	
	private double phoneNumber;

}
