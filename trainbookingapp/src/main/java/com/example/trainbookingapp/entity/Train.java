package com.example.trainbookingapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="train")
public class Train {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long trainId;
	
	private long trainNumber;
	
	private String trainName;
	
	private long seats;
	
	private long ticketPrice;
	
	private String source;
	
	private String destination;
	
	private String day;
	
	

}
