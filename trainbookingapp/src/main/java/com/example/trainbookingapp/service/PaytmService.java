package com.example.trainbookingapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.trainbookingapp.constants.AppConstants;

public class PaytmService implements PaymentService {

	
	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(PaytmService.class);
	
	@Override
	public String makePayment() {
		logger.info("Payment is done successfully with paytm");
		return AppConstants.SUCCESS;
	}

	@Override
	public String getServiceName() {
		return AppConstants.PaytmService;
	}

}
