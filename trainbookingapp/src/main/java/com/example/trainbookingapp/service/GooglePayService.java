package com.example.trainbookingapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.example.trainbookingapp.constants.AppConstants;
/**
 * 
 * @author sidramesh.mudhol
 *
 */
public class GooglePayService implements PaymentService {

	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(GooglePayService.class);
	
	@Override
	public String makePayment() {
		logger.info("Payment is done successfully with googlepay");
		return AppConstants.SUCCESS;
	}

	@Override
	public String getServiceName() {
		return AppConstants.GooglePayService;
	}

}
