package com.example.trainbookingapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trainbookingapp.constants.AppConstants;
import com.example.trainbookingapp.dto.BookingDetailDto;
import com.example.trainbookingapp.dto.BookingHistoryResponceDto;
import com.example.trainbookingapp.entity.BookingDetail;
import com.example.trainbookingapp.entity.User;
import com.example.trainbookingapp.exception.BookingDetailNotFoundException;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.repository.BookingDetailRepository;
import com.example.trainbookingapp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	BookingDetailRepository bookingDetailRepository;

	@Override
	public BookingHistoryResponceDto userBokingHistory(Long userId)
			throws UserExistsErrorException, BookingDetailNotFoundException {
		Optional<User> user = userRepository.findById(userId);

		if (!user.isPresent())
			throw new UserExistsErrorException(AppConstants.USER_NOT_FOUND);

		Optional<List<BookingDetail>> bookingDetailList = bookingDetailRepository.findByUser(user.get());

		if (!bookingDetailList.isPresent())
			throw new BookingDetailNotFoundException(AppConstants.HISTORY_NOT_FOUND);

		List<BookingDetailDto> bookingDetailDtosList = new ArrayList<>();
		BookingHistoryResponceDto bookingHistoryResponceDto = new BookingHistoryResponceDto();

		bookingDetailList.get().forEach(temp -> {
			BookingDetailDto bookingDetailDto = new BookingDetailDto();
			BeanUtils.copyProperties(temp, bookingDetailDto);
			bookingDetailDto.setTrainName(temp.getTrain().getTrainName());
			bookingDetailDto.setSource(temp.getTrain().getDestination());
			bookingDetailDto.setDestination(temp.getTrain().getDestination());
			bookingDetailDtosList.add(bookingDetailDto);
		});
		bookingHistoryResponceDto.setBookingDetailDtoList(bookingDetailDtosList);
		bookingHistoryResponceDto.setStatusMsg(AppConstants.HISTORY_DETAIL);
		bookingHistoryResponceDto.setStatusCode(900);
		return bookingHistoryResponceDto;
	}

}