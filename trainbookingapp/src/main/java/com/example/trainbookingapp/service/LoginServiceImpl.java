package com.example.trainbookingapp.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trainbookingapp.dto.LoginRequestDto;
import com.example.trainbookingapp.dto.LoginResponseDto;
import com.example.trainbookingapp.entity.User;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.exception.UserPasswordErrorException;
import com.example.trainbookingapp.repository.UserRepository;

/**
 * 
 * @author prava
 *
 */
@Service
public class LoginServiceImpl implements LoginService {
	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	@Autowired
	UserRepository userRepository;
	
    /**
     * 
     * @param loginRequestDto
	 * @return loginResponseDto
	 * @throws UserExistsErrorException
	 * @throws UserPasswordErrorException
     */
	
	@Override
	public LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws UserPasswordErrorException, UserExistsErrorException {
		
		logger.info("inside customerLogin method of CustomerServiceImpl class");
		Optional<User> userDetail1 = userRepository.findById(loginRequestDto.getUserId());
		if (!userDetail1.isPresent())
			throw new UserExistsErrorException("user not found");

		Optional<User> userDetail = userRepository.findByUserIdAndPassword(loginRequestDto.getUserId(),
				loginRequestDto.getPassword());
		if (!userDetail.isPresent())

			throw new UserPasswordErrorException("incorrect password");

		LoginResponseDto loginResponseDto = new LoginResponseDto();
		loginResponseDto.setUserId(loginRequestDto.getUserId());
		loginResponseDto.setStatusMsg("login success");
		loginResponseDto.setStatusCode(701);

		return loginResponseDto;
	}
}
