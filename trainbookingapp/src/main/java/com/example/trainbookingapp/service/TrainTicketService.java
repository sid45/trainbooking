package com.example.trainbookingapp.service;

import com.example.trainbookingapp.dto.TrainTicketRequestDto;
import com.example.trainbookingapp.dto.TrainTicketResponseDto;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.InvalidPaymentModeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.exception.UserNotFoundException;

/**
 * 
 * @author sidramesh.mudhol
 *
 */
public interface TrainTicketService {
	
	/**
	 * 
	 * This method is used to book train tickets.
	 * @param trainTicketRequestDto
	 * @return TrainTicketResponseDto
	 * @throws UserNotFoundException when invalid userId is given
	 * @throws NoSeatsAvailableException when available seats are less than required seats
	 * @throws TrainNotFoundException when invalid train id is given.
	 * @throws InvalidPaymentModeException when requested payment mode is not valid
	 * @throws DayBeforeException 
	 */
	public TrainTicketResponseDto bookTrainTicket(TrainTicketRequestDto trainTicketRequestDto) throws UserNotFoundException, NoSeatsAvailableException, TrainNotFoundException, InvalidPaymentModeException, DayBeforeException;

}
