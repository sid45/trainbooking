package com.example.trainbookingapp.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trainbookingapp.constants.AppConstants;
import com.example.trainbookingapp.dto.SearchResponceDto;
import com.example.trainbookingapp.dto.TrainDTO;
import com.example.trainbookingapp.dto.TrainDetailsDto;
import com.example.trainbookingapp.entity.SeatAvailability;
import com.example.trainbookingapp.entity.Train;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.repository.SeatAvailabilityRepository;
import com.example.trainbookingapp.repository.TrainRepository;
/**
 * 
 * @author supritha.p
 * @author krishna
 */
@Service
public class TrainServiceImpl implements TrainService {

	private static final Logger logger = LoggerFactory.getLogger(TrainServiceImpl.class);

	@Autowired
	private TrainRepository trainRepository;

	@Autowired
	private SeatAvailabilityRepository seatavailabilityRepository;

	@Override
	public TrainDTO getTraindetail(long trainId) throws TrainNotFoundException {

		Optional<Train> optr = trainRepository.findById(trainId);
		if (!optr.isPresent()) {
			throw new TrainNotFoundException(AppConstants.TRAIN_NOT_AVAILABLE);
		}
		TrainDTO tr = new TrainDTO();
		BeanUtils.copyProperties(optr.get(), tr);
		tr.setStatusCode(200);
		tr.setStatusMsg(AppConstants.TRAIN_FETCH);
		return tr;
	}
	
	@Override
	public SearchResponceDto searchTrain(String source, String destination, String date)
			throws DayBeforeException, TrainNotFoundException {

		logger.info("searchTrainDetails method of TrainServiceImpl class");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate userDate = LocalDate.parse(date, formatter);
		if (userDate.isBefore(LocalDate.now()))
			throw new DayBeforeException(AppConstants.DAY_BEFORE_TODAY);

		Optional<List<Train>> trainList = trainRepository.findBySourceContainingIgnoreCaseAndDestinationContainingIgnoreCaseAndDayContainingIgnoreCase(source, destination,
				userDate.getDayOfWeek().name());
		if (!trainList.isPresent())
			throw new TrainNotFoundException(AppConstants.TRAIN_NOT_AVAILABLE);

		List<TrainDetailsDto> trainDetailsDtoList = new ArrayList<>();
		trainList.get().forEach(temp -> {
			TrainDetailsDto trainDetailsDto = new TrainDetailsDto();
			BeanUtils.copyProperties(temp, trainDetailsDto);
			trainDetailsDtoList.add(trainDetailsDto);

		});

		SearchResponceDto searchResponceDto = new SearchResponceDto();
		searchResponceDto.setTrainDetailsList(trainDetailsDtoList);
		searchResponceDto.setStatusCode(AppConstants.TRAIN_LIST_STATUS_CODE);
		searchResponceDto.setStatusMsg(AppConstants.TRAIN_LIST);
		return searchResponceDto;
	}

	@Override
	public SeatAvailability confirmSeatAvailability(long numofseats, LocalDate date, long trainId)
			throws NoSeatsAvailableException, TrainNotFoundException {
		Optional<Train> optr = trainRepository.findById(trainId);
		if (!optr.isPresent()) {
			throw new TrainNotFoundException(AppConstants.TRAIN_NOT_AVAILABLE);
		}

		Optional<SeatAvailability> sat = seatavailabilityRepository.checktraindateexits(trainId, date);
		if (!sat.isPresent()) {

			Train tr = optr.get();
			SeatAvailability sa = new SeatAvailability();
			sa.setTrainDate(date);
			if (tr.getSeats() < numofseats) {
				throw new NoSeatsAvailableException(AppConstants.NO_SEAT_AVAILABLE);
			}
			sa.setAvailableSeats(tr.getSeats());
			sa.setTrain(tr);
			seatavailabilityRepository.save(sa);
			return sa;

		}

		SeatAvailability sa1 = sat.get();
		if (sa1.getAvailableSeats() <= 0 || sa1.getAvailableSeats() < numofseats) {
			throw new NoSeatsAvailableException(AppConstants.NO_SEAT_AVAILABLE);
		}

		return sa1;
	}
}
