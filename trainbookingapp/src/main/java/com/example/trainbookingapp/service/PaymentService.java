package com.example.trainbookingapp.service;

public interface PaymentService {
	
	public String makePayment();
	
	public String getServiceName();

}
