package com.example.trainbookingapp.service;

import com.example.trainbookingapp.dto.BookingHistoryResponceDto;
import com.example.trainbookingapp.exception.BookingDetailNotFoundException;
import com.example.trainbookingapp.exception.UserExistsErrorException;
/**
 * 
 * @author supritha.p
 *
 */
public interface UserService {
	/**
	 * @author supritha.p
	 * @param userId
	 * @return BookingHistoryResponceDto
	 * @throws UserExistsErrorException
	 * @throws BookingDetailNotFoundException
	 */

	BookingHistoryResponceDto userBokingHistory(Long userId)
			throws UserExistsErrorException, BookingDetailNotFoundException;
}
