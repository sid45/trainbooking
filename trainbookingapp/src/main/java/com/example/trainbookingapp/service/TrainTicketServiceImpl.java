package com.example.trainbookingapp.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.trainbookingapp.constants.AppConstants;
import com.example.trainbookingapp.dto.TrainTicketRequestDto;
import com.example.trainbookingapp.dto.TrainTicketResponseDto;
import com.example.trainbookingapp.entity.BookingDetail;
import com.example.trainbookingapp.entity.SeatAvailability;
import com.example.trainbookingapp.entity.Train;
import com.example.trainbookingapp.entity.User;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.InvalidPaymentModeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.exception.UserNotFoundException;
import com.example.trainbookingapp.helper.ServiceLocator;
import com.example.trainbookingapp.repository.BookingDetailRepository;
import com.example.trainbookingapp.repository.SeatAvailabilityRepository;
import com.example.trainbookingapp.repository.TrainRepository;
import com.example.trainbookingapp.repository.UserRepository;

/**
 * This class is TrainTicketService implementation class used to book train
 * tickets
 * 
 * @author sidramesh.mudhol
 *
 */
@Service
public class TrainTicketServiceImpl implements TrainTicketService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TrainRepository trainRepository;

	@Autowired
	private BookingDetailRepository bookingDetailRepository;

	@Autowired
	private SeatAvailabilityRepository seatAvailabilityRepository;

	@Autowired
	private TrainService trainService;
	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(TrainTicketServiceImpl.class);

	@Transactional
	@Override
	public TrainTicketResponseDto bookTrainTicket(TrainTicketRequestDto trainTicketRequestDto)
			throws UserNotFoundException, NoSeatsAvailableException, TrainNotFoundException,
			InvalidPaymentModeException, DayBeforeException {
		logger.info("inside bookTrainTicket() method");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate userDate = LocalDate.parse(trainTicketRequestDto.getBookingDate(), formatter);

		if (userDate.isBefore(LocalDate.now()))
			throw new DayBeforeException(AppConstants.DAY_BEFORE_TODAY);

		TrainTicketResponseDto trainTicketResponseDto = new TrainTicketResponseDto();
		Optional<User> optionalUser = userRepository.findById(trainTicketRequestDto.getUserId());
		if (!optionalUser.isPresent())
			throw new UserNotFoundException("user not found");

		Optional<Train> optionalTrain = trainRepository.findById(trainTicketRequestDto.getTrainId());
		if (!optionalTrain.isPresent()) {
			throw new TrainNotFoundException("train not found");
		}
		if (!optionalTrain.get().getDay().equalsIgnoreCase(userDate.getDayOfWeek().name()))
			throw new DayBeforeException(AppConstants.PASS_PROPER_DAY);

		synchronized (this) {
			logger.info("checking seat availability");
			SeatAvailability seatAvailability = trainService.confirmSeatAvailability(
					trainTicketRequestDto.getNoOfPassengers(), LocalDate.parse(trainTicketRequestDto.getBookingDate()),
					trainTicketRequestDto.getTrainId());
			logger.info("Making payment using service locator design pattern");
			String paymentMode = trainTicketRequestDto.getPaymentMode();
			PaymentService paymentService = ServiceLocator.getService(paymentMode);
			if (paymentService == null)
				throw new InvalidPaymentModeException("invalid payment mode");
			String paymentStatus = paymentService.makePayment();

			if ("success".equalsIgnoreCase(paymentStatus)) {
				BookingDetail detail = new BookingDetail();
				detail.setNoOfPassengers(trainTicketRequestDto.getNoOfPassengers());
				detail.setUser(optionalUser.get());
				detail.setTrain(optionalTrain.get());
				detail.setBookingDate(LocalDate.parse(trainTicketRequestDto.getBookingDate()));
				detail.setTotalPrice(
						(double) (optionalTrain.get().getTicketPrice() * trainTicketRequestDto.getNoOfPassengers()));
				detail.setPaymentMode(trainTicketRequestDto.getPaymentMode());
				bookingDetailRepository.save(detail);
				logger.info("update seat availability table");
				seatAvailability.setAvailableSeats(
						seatAvailability.getAvailableSeats() - trainTicketRequestDto.getNoOfPassengers());
				seatAvailabilityRepository.save(seatAvailability);

			}

		}

		trainTicketResponseDto.setStatusCode(700);
		trainTicketResponseDto.setStatusMsg("Train tickets booked successfully");
		logger.info("exiting bookTrainTicket() method");
		return trainTicketResponseDto;
	}

}
