package com.example.trainbookingapp.service;

import com.example.trainbookingapp.dto.LoginRequestDto;
import com.example.trainbookingapp.dto.LoginResponseDto;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.exception.UserPasswordErrorException;

public interface LoginService {

	LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws UserPasswordErrorException, UserExistsErrorException;

}
