package com.example.trainbookingapp.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.example.trainbookingapp.dto.SearchResponceDto;
import com.example.trainbookingapp.dto.TrainDTO;
import com.example.trainbookingapp.entity.SeatAvailability;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
/**
 * 
 * @author supritha.p
 * @author krishna
 */
@Service
public interface TrainService {
/**
 * @author krishna
 * @param trainId
 * @return TrainDTO
 * @throws TrainNotFoundException
 */
	TrainDTO getTraindetail(long trainId) throws TrainNotFoundException;

	/**
	 * @author krishna
	 * @param numofseats
	 * @param date
	 * @param trainId
	 * @return SeatAvailability
	 * @throws NoSeatsAvailableException
	 * @throws TrainNotFoundException
	 */
	SeatAvailability confirmSeatAvailability(long numofseats, LocalDate date, long trainId)
			throws NoSeatsAvailableException, TrainNotFoundException;
	/**
	 * @author supritha.p
	 * @param source
	 * @param destination
	 * @param userDate
	 * @return SearchResponceDto
	 * @throws DayBeforeException
	 * @throws TrainNotFoundException
	 */
	
	SearchResponceDto searchTrain(String source, String destination, String userDate)
			throws DayBeforeException, TrainNotFoundException;

}
