package com.example.trainbookingapp.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrainTicketRequestDto {
	
	private long userId;
	
	private long trainId;
	@NotNull
	@Min(value = 1, message="noOfPassengers should be equal or greater than 1")
	@Max(value = 5, message="noOfPassengers should be less than or equal to 5")
	private int noOfPassengers;
	
	private String bookingDate;
	
	private String paymentMode;

}
