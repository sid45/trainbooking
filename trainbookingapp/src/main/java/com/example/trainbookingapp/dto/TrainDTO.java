package com.example.trainbookingapp.dto;




import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrainDTO extends ResposeDto{
	
    private long trainId;
	
	private long trainNumber;
	
	private String trainName;
	
	private long ticketPrice;
	
	private String source;
	
	private String destination;
	
	

}
