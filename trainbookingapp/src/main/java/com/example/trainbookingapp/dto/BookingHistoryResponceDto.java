package com.example.trainbookingapp.dto;

import java.util.List;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BookingHistoryResponceDto extends ResposeDto {
	List<BookingDetailDto> bookingDetailDtoList;

}

