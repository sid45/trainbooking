package com.example.trainbookingapp.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequestDto{
	@NotBlank(message = "userid field mandatory")
	  private Long userId;
	@NotBlank(message = "password field mandatory")
	  private String password;
	  
}
