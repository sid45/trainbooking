package com.example.trainbookingapp.dto;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BookingDetailDto {
	private int noOfPassengers;

	private LocalDate bookingDate;

	private double totalPrice;

	private String paymentMode;
	private String source;

	private String destination;
	private String trainName;

}
