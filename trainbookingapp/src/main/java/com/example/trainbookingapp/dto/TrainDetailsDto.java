package com.example.trainbookingapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainDetailsDto {
private long trainId;
	
	private long trainNumber;
	
	private String trainName;
private String source;
	
	private String destination;

}
