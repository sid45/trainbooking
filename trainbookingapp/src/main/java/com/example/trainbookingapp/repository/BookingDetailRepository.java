package com.example.trainbookingapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.trainbookingapp.entity.BookingDetail;
import com.example.trainbookingapp.entity.User;

@Repository
public interface BookingDetailRepository extends CrudRepository<BookingDetail, Long> {

	Optional<List<BookingDetail>> findByUser(User user);

}
