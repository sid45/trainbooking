package com.example.trainbookingapp.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.trainbookingapp.entity.SeatAvailability;

@Repository
public interface SeatAvailabilityRepository extends CrudRepository<SeatAvailability, Long> {
	
	@Query(value = "select * from seatavailability where train_id = ?1 and train_date = ?2", nativeQuery = true)
	Optional<SeatAvailability> checktraindateexits(long tid,LocalDate dat);

}

