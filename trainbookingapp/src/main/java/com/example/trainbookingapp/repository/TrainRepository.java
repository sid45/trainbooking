package com.example.trainbookingapp.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.trainbookingapp.entity.Train;

@Repository
public interface TrainRepository extends CrudRepository<Train, Long> {



	Optional<List<Train>> findBySourceAndDestinationAndDay(String source, String destination, String weekOfDate);

	Optional<List<Train>> findBySourceContainingIgnoreCaseAndDestinationContainingIgnoreCaseAndDayContainingIgnoreCase(
			String source, String destination, String name);



}
