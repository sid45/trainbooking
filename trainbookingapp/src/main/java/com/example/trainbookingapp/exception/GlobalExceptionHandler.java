package com.example.trainbookingapp.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse error = new ErrorResponse(899l, details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(InvalidPaymentModeException.class)
	public final ResponseEntity<Object> handleValidationException(InvalidPaymentModeException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(900l, details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NoSeatsAvailableException.class)
	public ResponseEntity<ResponseStatus> handleSeatNotFoundException(NoSeatsAvailableException details) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(details.getLocalizedMessage());
		errorStatus.setStatuscode(901);
		return new ResponseEntity<>(errorStatus, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(TrainNotFoundException.class)
	public ResponseEntity<ResponseStatus> handleTrainNotFoundException(TrainNotFoundException details) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(details.getLocalizedMessage());
		errorStatus.setStatuscode(902);
		return new ResponseEntity<>(errorStatus, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(UserExistsErrorException.class)
	public final ResponseEntity<Object> handleValidationException(UserExistsErrorException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(801l, details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UserPasswordErrorException.class)
	public final ResponseEntity<Object> handleValidationException(UserPasswordErrorException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(809l, details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(DayBeforeException.class)
	public final ResponseEntity<Object> handleValidationException(DayBeforeException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(803l, details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BookingDetailNotFoundException.class)
	public final ResponseEntity<Object> handleValidationException(BookingDetailNotFoundException ex,
			WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(805l, details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

}
