package com.example.trainbookingapp.exception;

public class InvalidPaymentModeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidPaymentModeException(String message) {
		super(message);
	}

}
