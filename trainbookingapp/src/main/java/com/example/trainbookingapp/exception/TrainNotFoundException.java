package com.example.trainbookingapp.exception;

public class TrainNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainNotFoundException(String message) {
		super(message);
	}

}
