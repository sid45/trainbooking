package com.example.trainbookingapp.exception;

public class DayBeforeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	public DayBeforeException(String message) {
		super(message);
	}


}
