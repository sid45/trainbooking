package com.example.trainbookingapp.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trainbookingapp.dto.TrainTicketRequestDto;
import com.example.trainbookingapp.dto.TrainTicketResponseDto;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.InvalidPaymentModeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.exception.UserNotFoundException;
import com.example.trainbookingapp.service.TrainTicketService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This is a controller class used to book train tickets.
 * @author sidramesh.mudhol
 *
 */
@RestController
@RequestMapping("/tickets")
public class TrainTicketController {

	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(TrainTicketController.class);

	@Autowired
	private TrainTicketService trainTicketService;

	/**
	 * 
	 * @param trainTicketRequestDto
	 * @return
	 * @throws UserNotFoundException when invalid userId is given
	 * @throws NoSeatsAvailableException when available seats are less than required seats
	 * @throws TrainNotFoundException when invalid train id is given.
	 * @throws InvalidPaymentModeException when requested payment mode is not valid
	 * @throws DayBeforeException 
	 */
	@PostMapping("")
	@ApiOperation("booking train tickets")
	@ApiResponses(value = {@ApiResponse(responseContainer = "900", code = 900, message = "Invalid payment mode"),
            @ApiResponse(responseContainer = "901", code = 901, message = "seat not found"),
            @ApiResponse(responseContainer = "902", code = 902, message = "train not found"),
            @ApiResponse(responseContainer = "903", code = 903, message = "user not found"),
            @ApiResponse(responseContainer = "904", code = 904, message = "request field validation error"),
            @ApiResponse(responseContainer = "700", code = 700, message = "train tickets booked successfully")})
	public ResponseEntity<TrainTicketResponseDto> bookTrainTicket(@Valid @RequestBody TrainTicketRequestDto trainTicketRequestDto)
			throws UserNotFoundException, NoSeatsAvailableException, TrainNotFoundException,
			InvalidPaymentModeException, DayBeforeException {
		logger.info("inside bookTrainTicket() method");
		TrainTicketResponseDto trainTicketResponseDto = trainTicketService.bookTrainTicket(trainTicketRequestDto);
		logger.info("exiting bookTrainTicket() method");
		return new ResponseEntity<>(trainTicketResponseDto, HttpStatus.OK);

	}

}
