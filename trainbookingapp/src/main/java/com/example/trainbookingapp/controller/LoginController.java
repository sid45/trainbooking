package com.example.trainbookingapp.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.trainbookingapp.dto.LoginRequestDto;
import com.example.trainbookingapp.dto.LoginResponseDto;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.exception.UserPasswordErrorException;
import com.example.trainbookingapp.service.LoginService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This is controller class which is used for user login.
 * 
 * @author prava
 *
 */
@RestController
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	LoginService loginService;

	/**
	 * 
	 * @param loginRequestDto
	 * @return LoginResponseDto
	 * @throws UserExistsErrorException
	 * @throws UserPasswordErrorException
	 */

	@ApiResponses(value = { @ApiResponse( code = 800, message = "User not found"),
			@ApiResponse( code = 809, message = "Incorrect password") })
	@ApiOperation("User Login Operation")
	@PostMapping(value = "/login")
	public ResponseEntity<LoginResponseDto> checkLoginByUserId(@Valid @RequestBody LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException {
		logger.info("Inside UserController of user Login method ");
		return new ResponseEntity<>(loginService.userLogin(loginRequestDto), HttpStatus.ACCEPTED);
	}
}
