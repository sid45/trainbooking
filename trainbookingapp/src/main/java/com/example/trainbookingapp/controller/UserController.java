package com.example.trainbookingapp.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trainbookingapp.dto.BookingHistoryResponceDto;
import com.example.trainbookingapp.exception.BookingDetailNotFoundException;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * This is controller class for user.
 * @author supritha.p
 *
 */

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
	@Autowired
	UserService userService;
/**
 * 
 * @param userId
 * @return BookingHistoryResponceDto
 * @throws UserExistsErrorException
 * @throws BookingDetailNotFoundException
 */
	@ApiOperation("fetch train booking history")
	@ApiResponses(value = {
			@ApiResponse(responseContainer = "801", code = 0, message = "User not found"),
			@ApiResponse(responseContainer = "805", code = 0, message = "booking history is not present") })
	@GetMapping("/{userId}/history")
	public ResponseEntity<BookingHistoryResponceDto> userBokingHistory(@PathVariable Long userId)
			throws UserExistsErrorException, BookingDetailNotFoundException {
		return new ResponseEntity<>(userService.userBokingHistory(userId), HttpStatus.OK);
	}
}

