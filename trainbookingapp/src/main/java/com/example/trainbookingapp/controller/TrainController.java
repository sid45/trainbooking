package com.example.trainbookingapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.trainbookingapp.dto.TrainDTO;
import com.example.trainbookingapp.dto.SearchResponceDto;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.service.TrainService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author supritha.p
 * @author krishna
 */

@RestController
@RequestMapping("/")
@Api(value = "TrainController")
public class TrainController {
	private static final Logger logger = LoggerFactory.getLogger(TrainController.class);
	@Autowired
	private TrainService trainService;

	/**
	 * 
	 * @param trainId
	 * @return TrainDTO
	 * @throws TrainNotFoundException
	 */
	@GetMapping("/trains/{trainId}")
	@ApiResponses(value = {
			@ApiResponse(responseContainer = "800", code = 0, message = "train details are not avilable") })
	@ApiOperation("fetch train details")
	public ResponseEntity<TrainDTO> getTrainDetails(@PathVariable("trainId") long trainId)
			throws TrainNotFoundException {

		return new ResponseEntity<>(trainService.getTraindetail(trainId), HttpStatus.OK);

	}

	/**
	 * @author supritha.p
	 * @param source
	 * @param destination
	 * @param userDate
	 * @return SearchResponceDto
	 * @throws DayBeforeException
	 * @throws TrainNotFoundException
	 */
	@ApiOperation("searching the train")
	@ApiResponses(value = {
			@ApiResponse(responseContainer = "803", code = 0, message = "User cant search train for previous days"),
			@ApiResponse(responseContainer = "800", code = 0, message = "train details are not avilable") })
	@GetMapping("/trains")
	public ResponseEntity<SearchResponceDto> searchTrain(@RequestParam("source") String source,
			@RequestParam("destination") String destination, @RequestParam("userDate") String userDate)
			throws DayBeforeException, TrainNotFoundException {
		logger.info("Inside searchtrain method in TrainController");
		return new ResponseEntity<>(trainService.searchTrain(source, destination, userDate), HttpStatus.OK);
		
	}



}
