package com.example.trainbookingapp.constants;

public final class AppConstants {

	private AppConstants()
	{
		
	}


	public static final String DAY_BEFORE_TODAY = "you can only book for future days";
	public static final String TRAIN_LIST = "LIST OF TRAINS";
	public static final String USER_NOT_FOUND = "USER NOT FOUND";
	public static final String HISTORY_NOT_FOUND = "user not having any booking history";
	public static final String HISTORY_DETAIL = "booking list";
	public static final int TRAIN_LIST_STATUS_CODE = 900;

	public static final String CREDITCARDSERVICE = "CreditCardService";
	
	public static final String PaytmService = "PaytmService";
	
	public static final String GooglePayService = "GooglePayService";

	public static final String SUCCESS = "SUCCESS";
	
	public static final String CREDITCARD = "CreditCard";
	
	public static final String Paytm = "Paytm";
	
	public static final String GooglePay = "GooglePay";
	
	public static final String TRAIN_FETCH = "Success fetching train";
	public static final String TRAIN_NOT_AVAILABLE = "train not found";
	public static final String NO_SEAT_AVAILABLE = "Requested number of seats not available for this date";
	public static final String PASS_PROPER_DAY = "pass proper day";


}
