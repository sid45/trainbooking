package com.example.trainbookingapp.helper;

import com.example.trainbookingapp.constants.AppConstants;
import com.example.trainbookingapp.service.CreditCardService;
import com.example.trainbookingapp.service.GooglePayService;
import com.example.trainbookingapp.service.PaytmService;

public class InitialContext {
	
	public Object lookup(String serviceName) {
        if (serviceName.equalsIgnoreCase(AppConstants.Paytm)) {
            return new PaytmService();
        } else if (serviceName.equalsIgnoreCase(AppConstants.GooglePay)) {
            return new GooglePayService();
        } else if (serviceName.equalsIgnoreCase(AppConstants.CREDITCARD)) {
            return new CreditCardService();
        }
        return null;
    }

}
