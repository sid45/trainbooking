package com.example.trainbookingapp.helper;

import com.example.trainbookingapp.service.PaymentService;

public class ServiceLocator {
	private ServiceLocator() 
	{

	}
	public static PaymentService getService(String serviceName) {

		InitialContext context = new InitialContext();
		return (PaymentService) context.lookup(serviceName);
	}

}
