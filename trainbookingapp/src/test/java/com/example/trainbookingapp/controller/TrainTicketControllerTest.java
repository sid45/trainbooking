package com.example.trainbookingapp.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.trainbookingapp.dto.TrainTicketRequestDto;
import com.example.trainbookingapp.dto.TrainTicketResponseDto;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.InvalidPaymentModeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.exception.UserNotFoundException;
import com.example.trainbookingapp.service.TrainTicketService;

@SpringBootTest
class TrainTicketControllerTest {

	@Mock
	TrainTicketService trainTicketService;

	@InjectMocks
	TrainTicketController trainTicketController;
	
	
	
	@Test
	public void testBookTrainTicket() throws UserNotFoundException, NoSeatsAvailableException, TrainNotFoundException, InvalidPaymentModeException, DayBeforeException {
		TrainTicketRequestDto trainTicketRequestDto = new TrainTicketRequestDto();
		TrainTicketResponseDto trainTicketResponseDto = new TrainTicketResponseDto();
		trainTicketResponseDto.setStatusCode(700);
		Mockito.when(trainTicketService.bookTrainTicket(Mockito.any(TrainTicketRequestDto.class))).thenReturn(trainTicketResponseDto);
		ResponseEntity<TrainTicketResponseDto> response = trainTicketController.bookTrainTicket(trainTicketRequestDto);
		Assertions.assertEquals(700, response.getBody().getStatusCode());

	
	}

}
