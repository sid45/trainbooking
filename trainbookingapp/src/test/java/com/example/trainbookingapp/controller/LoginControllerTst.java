package com.example.trainbookingapp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import com.example.trainbookingapp.service.LoginService;
import com.example.trainbookingapp.dto.LoginRequestDto;
import com.example.trainbookingapp.dto.LoginResponseDto;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.exception.UserPasswordErrorException;

@SpringBootTest
class LoginControllerTst {
	@Mock
	LoginService loginService;

	@InjectMocks
	LoginController loginController;

	LoginResponseDto userLoginResponseDto;

	LoginRequestDto loginRequestDto;

	@BeforeEach
	public void setup() {
		userLoginResponseDto = new LoginResponseDto();
		userLoginResponseDto.setStatusCode(701);
		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setUserId(1234L);
		loginRequestDto.setPassword("prava");

	}

	@Test
	void LoginControllerTest() throws UserPasswordErrorException, UserExistsErrorException {
		Mockito.when(loginService.userLogin(Mockito.any(LoginRequestDto.class))).thenReturn(userLoginResponseDto);
		ResponseEntity<LoginResponseDto> entity = loginController.checkLoginByUserId(loginRequestDto);
		Assertions.assertEquals(userLoginResponseDto.getStatusCode(), entity.getBody().getStatusCode());

	}

}
