package com.example.trainbookingapp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.trainbookingapp.dto.BookingHistoryResponceDto;
import com.example.trainbookingapp.exception.BookingDetailNotFoundException;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.service.UserService;

@SpringBootTest
class UserControllerTest {
	@Mock
	UserService userService;
	@InjectMocks
	UserController userController;

	BookingHistoryResponceDto bookingHistoryResponceDto;

	@BeforeEach
	public void setup() {
		bookingHistoryResponceDto = new BookingHistoryResponceDto();
	}

	@Test
	void userBokingHistory() throws UserExistsErrorException, BookingDetailNotFoundException {
		Mockito.when(userService.userBokingHistory(Mockito.anyLong())).thenReturn(bookingHistoryResponceDto);
		ResponseEntity<BookingHistoryResponceDto> result = userController.userBokingHistory(Mockito.anyLong());
		Assertions.assertEquals(bookingHistoryResponceDto.getStatusCode(), result.getBody().getStatusCode());
	}

}
