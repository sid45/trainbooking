package com.example.trainbookingapp.controller;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.trainbookingapp.dto.SearchResponceDto;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.service.TrainService;

@SpringBootTest
class TrainControllerTest {

	@Mock
	TrainService trainService;

	@InjectMocks
	TrainController trainController;

	SearchResponceDto searchResponceDto;

	@BeforeEach
	public void setup() {
		searchResponceDto = new SearchResponceDto();
		searchResponceDto.setStatusCode(800);
	}

	@Test
	void searchTrain() throws DayBeforeException, TrainNotFoundException {
		String destination = "mangalore", source = "bangalore", date = "2020-10-10";
		Mockito.when(trainService.searchTrain(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
				.thenReturn(searchResponceDto);
		ResponseEntity<SearchResponceDto> result = trainController.searchTrain(destination, source, date);
		Assertions.assertEquals(searchResponceDto.getStatusCode(), result.getBody().getStatusCode());
	}

}
