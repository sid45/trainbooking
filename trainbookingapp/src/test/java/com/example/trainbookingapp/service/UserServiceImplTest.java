package com.example.trainbookingapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trainbookingapp.dto.BookingHistoryResponceDto;
import com.example.trainbookingapp.entity.BookingDetail;
import com.example.trainbookingapp.entity.Train;
import com.example.trainbookingapp.entity.User;
import com.example.trainbookingapp.exception.BookingDetailNotFoundException;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.repository.BookingDetailRepository;
import com.example.trainbookingapp.repository.UserRepository;

@SpringBootTest
class UserServiceImplTest {
	@Mock
	UserRepository userRepository;

	@Mock
	BookingDetailRepository bookingDetailRepository;

	@InjectMocks
	UserServiceImpl userServiceImpl;

	User user;
	List<BookingDetail> bookingDetailList;
	BookingDetail bookingDetail;
	Train train;

	@BeforeEach
	public void setup() {
		user = new User();
		bookingDetailList = new ArrayList<>();
		bookingDetail = new BookingDetail();

		train = new Train();
		train.setSource("mangalore");
		train.setDestination("bangalore");
		train.setTrainName("subbalakshmi");
		bookingDetail.setTrain(train);
		bookingDetailList.add(bookingDetail);

	}

	@Test
	void userBokingHistory() throws UserExistsErrorException, BookingDetailNotFoundException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.when(bookingDetailRepository.findByUser(Mockito.any(User.class)))
				.thenReturn(Optional.of(bookingDetailList));
		BookingHistoryResponceDto result = userServiceImpl.userBokingHistory(Mockito.anyLong());
		Assertions.assertEquals("subbalakshmi", result.getBookingDetailDtoList().get(0).getTrainName());
	}
	
	@Test
	void userBokingHistoryUserExistsErrorException() throws UserExistsErrorException, BookingDetailNotFoundException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Assertions.assertThrows(UserExistsErrorException.class,()-> userServiceImpl.userBokingHistory(Mockito.anyLong()));
	}
	
	@Test
	void userBokingHistoryBookingDetailNotFoundException() throws UserExistsErrorException, BookingDetailNotFoundException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.when(bookingDetailRepository.findByUser(Mockito.any(User.class)))
		.thenReturn(Optional.empty());
		Assertions.assertThrows(BookingDetailNotFoundException.class,()-> userServiceImpl.userBokingHistory(Mockito.anyLong()));
	}

}
