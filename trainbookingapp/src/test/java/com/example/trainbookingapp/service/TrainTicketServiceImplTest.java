package com.example.trainbookingapp.service;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trainbookingapp.dto.TrainTicketRequestDto;
import com.example.trainbookingapp.dto.TrainTicketResponseDto;
import com.example.trainbookingapp.entity.SeatAvailability;
import com.example.trainbookingapp.entity.Train;
import com.example.trainbookingapp.entity.User;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.InvalidPaymentModeException;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.exception.UserNotFoundException;
import com.example.trainbookingapp.repository.BookingDetailRepository;
import com.example.trainbookingapp.repository.SeatAvailabilityRepository;
import com.example.trainbookingapp.repository.TrainRepository;
import com.example.trainbookingapp.repository.UserRepository;

@SpringBootTest
class TrainTicketServiceImplTest {

	@InjectMocks
	TrainTicketServiceImpl trainTicketImpl;
	
	@Mock
	private UserRepository userRepository;

	@Mock
	private TrainRepository trainRepository;

	@Mock
	private BookingDetailRepository bookingDetailRepository;

	@Mock
	private SeatAvailabilityRepository seatAvailabilityRepository;

	@Mock
	private TrainService trainService;
	
	TrainTicketRequestDto trainTicketRequestDto = null;
	User user = null;
	Train train = null;
	
	@BeforeEach
	public void setUp() {
		user = new User();
		user.setEmail("email");
		train = new Train();
		train.setTrainName("express");
		train.setTicketPrice(100);
		trainTicketRequestDto = new TrainTicketRequestDto();
		trainTicketRequestDto.setPaymentMode("Paytm");
		trainTicketRequestDto.setBookingDate("2020-07-25");
		trainTicketRequestDto.setNoOfPassengers(3);
		
	}
	@Test
	public void testBookTrainTicket() throws NoSeatsAvailableException, TrainNotFoundException, UserNotFoundException, InvalidPaymentModeException, DayBeforeException {
		
		Optional<User> optionalUser = Optional.of(user);
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(optionalUser);
		Optional<Train> optionalTrain = Optional.of(train);
		optionalTrain.get().setDay("SATURDAY");
		Mockito.when(trainRepository.findById(Mockito.anyLong())).thenReturn(optionalTrain);
		SeatAvailability seatAvailability = new SeatAvailability();
		Mockito.when(trainService.confirmSeatAvailability(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyLong())).thenReturn(seatAvailability);
		TrainTicketResponseDto response = trainTicketImpl.bookTrainTicket(trainTicketRequestDto);
		Assertions.assertEquals(700, response.getStatusCode());
	}
	
	@Test
	public void testBookTrainTicketForUserNotFoundException() throws NoSeatsAvailableException, TrainNotFoundException, UserNotFoundException, InvalidPaymentModeException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Assertions.assertThrows(UserNotFoundException.class,
                () ->  trainTicketImpl.bookTrainTicket(trainTicketRequestDto));

	}
	
	@Test
	public void testBookTrainTicketForTrainNotFoundException() throws NoSeatsAvailableException, TrainNotFoundException, UserNotFoundException, InvalidPaymentModeException {
		Optional<User> optionalUser = Optional.of(user);
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(optionalUser);
		Mockito.when(trainRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Assertions.assertThrows(TrainNotFoundException.class,
                () ->  trainTicketImpl.bookTrainTicket(trainTicketRequestDto));

	}
	
	@Test
	public void testBookTrainTicketInvalidPaymentModeException() throws NoSeatsAvailableException, TrainNotFoundException, UserNotFoundException, InvalidPaymentModeException {
		Optional<User> optionalUser = Optional.of(user);
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(optionalUser);
		Optional<Train> optionalTrain = Optional.of(train);
		optionalTrain.get().setDay("SATURDAY");
		Mockito.when(trainRepository.findById(Mockito.anyLong())).thenReturn(optionalTrain);
		SeatAvailability seatAvailability = new SeatAvailability();
		trainTicketRequestDto.setPaymentMode("invalidpaymentmode");
		
		Mockito.when(trainService.confirmSeatAvailability(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyLong())).thenReturn(seatAvailability);
		Assertions.assertThrows(InvalidPaymentModeException.class,
                () ->  trainTicketImpl.bookTrainTicket(trainTicketRequestDto));

	}
	
	
	@Test
	public void testBookTrainTicketForDayBeforeException() throws NoSeatsAvailableException, TrainNotFoundException, UserNotFoundException, InvalidPaymentModeException {
		trainTicketRequestDto.setBookingDate("2020-07-22");
		Assertions.assertThrows(DayBeforeException.class,
                () ->  trainTicketImpl.bookTrainTicket(trainTicketRequestDto));

	}
	

}
