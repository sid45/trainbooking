package com.example.trainbookingapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trainbookingapp.dto.TrainDTO;
import com.example.trainbookingapp.entity.SeatAvailability;
import com.example.trainbookingapp.entity.Train;
import com.example.trainbookingapp.exception.NoSeatsAvailableException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.repository.SeatAvailabilityRepository;
import com.example.trainbookingapp.repository.TrainRepository;

@SpringBootTest
public class TrainServiceTest {
	
	@InjectMocks
	TrainServiceImpl trainServiceimpl;

	@Mock
	TrainRepository trainRepository;
	
	@Mock
	SeatAvailabilityRepository seatavailabilityRepository;
	
	Train t = new Train();
	long on = 1;
	LocalDate ld = LocalDate.parse("2017-11-10");
	SeatAvailability sa = new SeatAvailability();
	SeatAvailability sa1 = new SeatAvailability();
	TrainDTO tr = new TrainDTO();
	@BeforeEach
	public void init() {
		
		
		t.setDay("monday");
		t.setDestination("bangalore");
		t.setSeats(1000);
		t.setSource("chennai");
		t.setTicketPrice(100);
		t.setTrainId(1);
		t.setTrainName("chennai mail");
		t.setTrainNumber(123);
		sa.setAvailableSeats(100);
		sa.setTrain(t);
		sa.setTrainDate(ld);
		sa.setTrainDetailsId(1);
		sa1.setAvailableSeats(2);
		sa1.setTrain(t);
		sa1.setTrainDate(ld);
		sa1.setTrainDetailsId(1);
		
	}
	
	
	@Test
	public void TestgetTraindetail() throws TrainNotFoundException  {
		
		when(trainRepository.findById(on)).thenReturn(Optional.of(t));
		TrainDTO result = trainServiceimpl.getTraindetail(on);
		assertEquals("bangalore", result.getDestination());
	}
	
	
	@Test
	public void TestconfirmSeatAvailability() throws TrainNotFoundException,NoSeatsAvailableException {
		
		
		Optional<SeatAvailability> empty = Optional.empty();
		when(trainRepository.findById(on)).thenReturn(Optional.of(t));
		when(seatavailabilityRepository.checktraindateexits(1,ld)).thenReturn(empty);
		SeatAvailability result = trainServiceimpl.confirmSeatAvailability(5, ld, 1);
		assertEquals(1000, result.getAvailableSeats());
		
		
	}
	
	
	@Test
	public void TestconfirmSeatAvailability1() throws TrainNotFoundException,NoSeatsAvailableException {
		
		
		
		
		
		when(trainRepository.findById(on)).thenReturn(Optional.of(t));
		when(seatavailabilityRepository.checktraindateexits(1,ld)).thenReturn(Optional.of(sa));
		SeatAvailability result = trainServiceimpl.confirmSeatAvailability(5, ld, 1);
		assertEquals(100, result.getAvailableSeats());
		
		
	}
	
	@Test
	public void TestconfirmSeatAvailabilitynegatives() throws TrainNotFoundException,NoSeatsAvailableException {
	
		try {
		Optional<Train> empty = Optional.empty();
		when(trainRepository.findById(on)).thenReturn(empty);
		when(seatavailabilityRepository.checktraindateexits(1,ld)).thenReturn(Optional.of(sa));
		trainServiceimpl.confirmSeatAvailability(5, ld, 1);
		}
	  catch (TrainNotFoundException result) {

         Assertions.assertEquals("train not found",result.getLocalizedMessage());
     }
		
	}
	
	
	@Test
	public void TestconfirmSeatAvailabilitynegatives1() throws TrainNotFoundException,NoSeatsAvailableException {
	
		try {
		
		when(trainRepository.findById(on)).thenReturn(Optional.of(t));
		when(seatavailabilityRepository.checktraindateexits(1,ld)).thenReturn(Optional.of(sa1));
		trainServiceimpl.confirmSeatAvailability(5, ld, 1);
		}
	  catch (NoSeatsAvailableException result) {

         Assertions.assertEquals("Requested number of seats not available for this date",result.getLocalizedMessage());
     }
		
	}
	
	
	
	@Test
	public void TestgetTraindetailneg() throws TrainNotFoundException  {
		
		try {
			Optional<Train> empty = Optional.empty();
			when(trainRepository.findById(on)).thenReturn(empty);
			trainServiceimpl.getTraindetail(on);
		}
		
		catch (TrainNotFoundException train) {

	         Assertions.assertEquals("train not found",train.getLocalizedMessage());
	     }
	}
	
	
}
