package com.example.trainbookingapp.service;

import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.trainbookingapp.dto.LoginRequestDto;
import com.example.trainbookingapp.dto.LoginResponseDto;
import com.example.trainbookingapp.entity.User;
import com.example.trainbookingapp.exception.UserExistsErrorException;
import com.example.trainbookingapp.exception.UserPasswordErrorException;
import com.example.trainbookingapp.repository.UserRepository;

@SpringBootTest
class LoginServiceTest {
	@Mock
	UserRepository userRepository;
	@InjectMocks
	LoginServiceImpl LoginServiceImpl;

	LoginResponseDto userLoginResponseDto;

	LoginRequestDto loginRequestDto;
	User user;

	/**
	 * Intial setup
	 */
	@BeforeEach
	public void setup() {
		userLoginResponseDto = new LoginResponseDto();
		userLoginResponseDto.setStatusCode(701);
		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setUserId(1234L);
		loginRequestDto.setPassword("prava");

		user = new User();
	}

	@Test
	void userLoginTest() throws UserPasswordErrorException, UserExistsErrorException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.when(userRepository.findByUserIdAndPassword(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.of(user));
		LoginResponseDto result = LoginServiceImpl.userLogin(loginRequestDto);
		Assertions.assertEquals(701, result.getStatusCode());
	}

	@Test
	void userLoginTestUserPasswordErrorException() throws UserPasswordErrorException, UserExistsErrorException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		Mockito.when(userRepository.findByUserIdAndPassword(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.empty());
		Assertions.assertThrows(UserPasswordErrorException.class, () -> LoginServiceImpl.userLogin(loginRequestDto));
	}

	@Test
	void userLoginTestUserExistsErrorException() throws UserPasswordErrorException, UserExistsErrorException {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Mockito.when(userRepository.findByUserIdAndPassword(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.empty());
		Assertions.assertThrows(UserExistsErrorException.class, () -> LoginServiceImpl.userLogin(loginRequestDto));
	}
}
