package com.example.trainbookingapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trainbookingapp.dto.SearchResponceDto;
import com.example.trainbookingapp.entity.Train;
import com.example.trainbookingapp.exception.DayBeforeException;
import com.example.trainbookingapp.exception.TrainNotFoundException;
import com.example.trainbookingapp.repository.TrainRepository;

@SpringBootTest
class TrainServiceImplTest {
	@Mock
	TrainRepository trainRepository;
	@InjectMocks
	TrainServiceImpl trainServiceImpl;

	List<Train> trainList;
	Train train;

	@BeforeEach
	public void setup() {
		trainList = new ArrayList<>();
		train = new Train();
		trainList.add(train);
	}

	@Test
	void searchTrainTest() throws DayBeforeException, TrainNotFoundException {
		String destination = "mangalore", source = "bangalore", date = "2020-07-28";
		Mockito.when(trainRepository.findBySourceContainingIgnoreCaseAndDestinationContainingIgnoreCaseAndDayContainingIgnoreCase(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.of(trainList));
		SearchResponceDto result = trainServiceImpl.searchTrain(destination, source, date);
		Assertions.assertEquals(900, result.getStatusCode());
	}
	
	@Test
	void searchTrainTestDayBeforeException() throws DayBeforeException, TrainNotFoundException {
		String destination = "mangalore", source = "bangalore", date = "2020-07-20";
		Assertions.assertThrows(DayBeforeException.class,() -> trainServiceImpl.searchTrain(destination, source, date));
	}
	
	@Test
	void searchTrainTestTrainNotFoundException() throws DayBeforeException, TrainNotFoundException {
		String destination = "mangalore", source = "bangalore", date = "2020-07-28";
		Mockito.when(trainRepository.findBySourceContainingIgnoreCaseAndDestinationContainingIgnoreCaseAndDayContainingIgnoreCase(Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString())).thenReturn(Optional.empty());
		Assertions.assertThrows(TrainNotFoundException.class,() -> trainServiceImpl.searchTrain(destination, source, date));
	}

}
